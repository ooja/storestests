package storestests

import (
	"testing"
	conn "bitbucket.org/ooja/crawler/connections"
	"bitbucket.org/ooja/crawler/worker"
	"bitbucket.org/ooja/common"
	"os"
	"bitbucket.org/ooja/pagequery"
	"encoding/json"
	."gopkg.in/check.v1"
	"fmt"
	"strings"

)

var mongoDBConStr = "localhost"
var mongoDBName = "ooja"
var mongoDBColName = "test_store_list"
var storeNames = []string{ "drinksng", "konga", "jumia", "kaymu", "payporte", "jayosbie", "smartbuy", "ella",}
var HTMLCache map[string]string

func Log(args ...interface{}){
	fmt.Println(args...)
}

// maps stores to thier selectors and their respective test links
var StoreTestData = map[string]map[string]string{
	"konga": map[string]string{
		"default": "http://www.konga.com/hp-stream-14-quad-core-2gb-32gb-ssd-14-inch-windows-8-1-laptop-free-airtel-modem-1544441",
		"availableSizes": "http://www.konga.com/gola-mens-aubrey-black-weave-1278222",
		"category": "http://www.konga.com/samsung-phones",
	},
	"jumia": map[string]string{
		"default": "http://www.jumia.com.ng/hp-250-g3-intel-celeron-2.16ghz-2gb500gb-hdd-15.6-inch-windows-8-laptop-193100.html",
		"availableSizes": "http://www.jumia.com.ng/lantin-formal-longsleeve-shirt-with-ruffle-and-lace-detail-black-146487.html",
		"regularPrice": "http://www.jumia.com.ng/ferrari-black-cologne-100ml-edp-perfume-for-men-122852.html",
		"category": "http://www.jumia.com.ng/phones-tablets?setDevice=mobile",
	},
	"kaymu": map[string]string{
		"default": "http://www.kaymu.com.ng/dell-latitude-d610-with-atleast-40hdd-and-1gb-ram-308319.html",
		"availableSizes": "http://www.kaymu.com.ng/royal-blue-peplum-dress-260275.html",
		"regularPrice": "http://www.kaymu.com.ng/royal-blue-peplum-dress-260275.html",
		"specialPrice": "http://www.kaymu.com.ng/royal-blue-peplum-dress-260275.html",
		"category": "http://www.kaymu.com.ng/men-s-shoes/",
	},
	"payporte": map[string]string{
		"default": "http://www.payporte.com/phones-accessories/mobile-phones/phones/archos-45-neon.html",
		"category": "http://www.payporte.com/women-307/tops/blouses.html",
	},
	"olx": map[string]string{
		"default": "http://lagos-island-west.olx.com.ng/very-neat-blackberry-q10-iid-809796289",
		"otherImages": "http://ejigbo.olx.com.ng/white-samsung-duos-iid-809795656",
	},
	"fashpa": map[string]string{
		"default": "http://www.fashpa.com/o-eclat-keira-tassle-clutch-8676.html",
	},
	"jayosbie": map[string]string{
		"default": "http://jayosbie.com/tops/ello-bae-tees-white",
		"category": "http://jayosbie.com/shirts",
	},
	"smartbuy": map[string]string{
		"default": "http://www.smartbuy.ng/shop-all-categories/mobile-phone-tablets/cell-phones/tecno-p6-phantom-mini.html",
		"shortDescription": "http://www.smartbuy.ng/silex-m301-mobile-phone.html",
		"specialPrice": "http://www.smartbuy.ng/silex-m301-mobile-phone.html",
		"category": "http://www.smartbuy.ng/shop-all-categories/computers/laptops.html",
	},
	"ella": map[string]string{
		"default": "http://www.ella.ng/blouses/emb20103.html",
		"specialPrice": "http://www.ella.ng/trousers/emt50054.html",
		"category": "http://www.ella.ng/blouses.html",
	},
	"drinksng": map[string]string{
		"default": "http://drinks.ng/wine/non-alcoholic/Flemish",
		"category": "http://drinks.ng/wine/non-alcoholic",
	},
}

func init (){

	// in continious integration environment
	if os.Getenv("APP_ENV") == "CI" {
		mongoUser := os.Getenv("MONGO_USER")
		mongoPass := os.Getenv("MONGO_PASS")
		mongoDBName = "heroku_app37256971"
		mongoDBConStr = "mongodb://" + mongoUser + ":" + mongoPass + "@ds031902.mongolab.com:31902/" + mongoDBName
	}

	// limit test to certain stores.
	// separate store names by spaces
	if os.Getenv("LIMIT_TO") != "" {
		storeNames = strings.Fields(os.Getenv("LIMIT_TO"))
	}

	// set collection name from ENV
	if os.Getenv("STORE_COL") != "" {
		mongoDBColName = os.Getenv("STORE_COL")
	}

	conn.ConnectToMongoDB(&mongoDBConStr)
}

type TestSuite struct{
	stores []common.Store
}

var _ = Suite(&TestSuite{})

func Test(t *testing.T) { 
	TestingT(t) 
}

func (s *TestSuite) SetUpSuite (c *C) {

	// get all stores
	s.stores = GetStores(storeNames, mongoDBName, mongoDBColName)

	// stores should be a slice of stores
	c.Assert(s.stores, FitsTypeOf, []common.Store{})

	// stores count should equal expected
	c.Assert(len(s.stores), Equals, len(storeNames))

	// prefetch selector listings url
	s.ExplicitTestPrefetchListings(c)
}

// Explicitly call this function to test a store's category
// returns category page content if it passes
func (s *TestSuite) ExplicitTestStoresCategory (c *C, store common.Store) string {

		// download store homepage
		wrker := worker.Worker{}
		categories, err := wrker.GetCategoryLinks(&store)
		c.Assert(err, IsNil)

		// store must have categories
		c.Assert(len(categories), Not(Equals), 0)

		// get test category url of category selector
		body := StoreTestData[store.Name]["category"]

		// test category body must be set. Ensure a category test link is provided in StoreTestData
		c.Assert(body, Not(Equals), "")

		return body
}

func (s *TestSuite) TestPageSelector(c *C) {

	for _, store := range s.stores {

		body := s.ExplicitTestStoresCategory(c, store)

		// check selectors, it must be a map
		selectors := store.Selectors
		c.Assert(selectors, FitsTypeOf, map[string]interface{}{})
		
		// create worker
		wrker := worker.Worker{}

		// selectors map must have a `page` field/selector
		pageSelector := selectors["page"]

		// selector type should be a map of string key and interface{} value
		c.Assert(pageSelector, NotNil)

		// page seletor must be string
		c.Assert(pageSelector, FitsTypeOf, "")

		// page selector must not be empty
		c.Assert(len(pageSelector.(string)), Not(Equals), 0)

		// extract page links from category body
		Log("Extracting pages from store: ", store.Name)
		pageUrlMap, err := wrker.GetPageLinks(body, pageSelector.(string), store.Domain)
		c.Assert(err, IsNil)

		// page url map must be a map of string key and empty struct{} as value
		c.Assert(pageUrlMap, FitsTypeOf, map[string]struct{}{})

		// page url map must contain atleast one item
		c.Assert(len(pageUrlMap), Not(Equals), 0)

		// get first page url
		firstPageUrl := GetFirstMapKey(pageUrlMap)

		// ensure first page url is a url
		c.Assert(StartsWith("http", firstPageUrl), Equals, true)
	}
}

func (s *TestSuite) TestListingSelector(c *C) {

	for _, store := range s.stores {

		body := s.ExplicitTestStoresCategory(c, store)
		
		// check selectors, it must be a map
		selectors := store.Selectors

		// selector type should be a map of string key and interface{} value
		c.Assert(selectors, FitsTypeOf, map[string]interface{}{})
		
		// create worker
		wrker := worker.Worker{}
		listingSelector := selectors["listing"]

		// listing select must be present
		c.Assert(listingSelector, NotNil)

		// listing selector must be string
		c.Assert(listingSelector, FitsTypeOf, "")

		// listing selector must not be empty
		c.Assert(len(listingSelector.(string)), Not(Equals), 0)
		listingUrlMap, err := wrker.GetListing(body, listingSelector.(string), store.Domain)
		
		// must be nil
		c.Assert(err, IsNil)

		// listing url map must be a map of string key and empty struct{} as value
		c.Assert(listingUrlMap, FitsTypeOf, map[string]struct{}{})

		// listing url map must contain atleast one item
		Log("Extracting listings from store: ", store.Name)
		c.Assert(len(listingUrlMap), Not(Equals), 0)

		// get first listing
		firstListingUrl := GetFirstMapKey(listingUrlMap)

		// ensure first listing is a url
		c.Assert(StartsWith("http", firstListingUrl), Equals, true)
	}
}

// prefetches the store listing urls stored in StoreTestData
// replaces each individual selector with its corresponding html content
func (s *TestSuite) ExplicitTestPrefetchListings(c *C) {
	HTMLCache = make(map[string]string)
	for _, name := range storeNames {
		if selectorListingMap := StoreTestData[name]; selectorListingMap != nil {
			for selector, listingUrl := range selectorListingMap {

				// if url has not been downloaded and cached before, download it
				if HTMLCache[listingUrl] == "" {

					Log("Fetching url: ", listingUrl)

					// download the listing Url
					body, statusCode, err := FetchURL(listingUrl)
					c.Assert(err, IsNil)
					if statusCode == 403 {
						Log(body)
					}
					// status code must be 200
					c.Assert(statusCode, Equals, 200)

					// body must be string
					c.Assert(body, FitsTypeOf, "")

					// body must not be empty
					c.Assert(len(body), Not(Equals), 0)

					// add to cache
					HTMLCache[listingUrl] = body

				}

				// replace listing url in selector map with body
				selectorListingMap[selector] = strings.TrimSpace(HTMLCache[listingUrl])
			}
		}
	}
}



func (s *TestSuite) TestStoreSelectors (c *C) {

	for _, store := range s.stores {

		// store name must be present in selector listing map
		c.Assert(StoreTestData[store.Name], NotNil)

		// get default test listing page content for store
		defaultTestListing := StoreTestData[store.Name]["default"]
		c.Assert(StartsWith("<!doctype", strings.ToLower(defaultTestListing[0: 50])), Equals, true)

		// check selectors, it must be a map
		selectors := store.Selectors

		// selector type should be a map of string key and interface{} value
		c.Assert(selectors, FitsTypeOf, map[string]interface{}{})

		delete(selectors, "listing")
		delete(selectors, "page")
		delete(selectors, "category")

		for selectorName, selectorValue := range selectors {

			// selector value must be a map of string key and interface{} value
			c.Assert(selectorValue, FitsTypeOf, map[string]interface{}{})

			// cast to original type
			actualSelectorValue := selectorValue.(map[string]interface{})

			// selector value must have a selector field
			cssSelector := actualSelectorValue["selector"]
			c.Assert(cssSelector, NotNil)

			// selector value must have a `funcs` field
			c.Assert(actualSelectorValue["funcs"], NotNil)

			// use specified selector listing content/html as listing url if present
			listingHTML := defaultTestListing		
			if StoreTestData[store.Name][selectorName] != "" {
				listingHTML = StoreTestData[store.Name][selectorName]
			}	

			// create a new map object holding just the current selector data
			exclusiveSelector := make(map[string]interface{})
			exclusiveSelector[selectorName] = selectors[selectorName]

			// convert selectors map object to json
			selectorStr, err := json.Marshal(exclusiveSelector)
			c.Assert(err, IsNil)

			// selectorStr must be a string
			c.Assert(string(selectorStr), FitsTypeOf, "")

			// parse listing content
			pageQuery := pagequery.PageQuery{}
			result, err := pageQuery.Read(listingHTML, string(selectorStr))
			c.Assert(err, IsNil)

			if result["title"] != nil {
				// title to be a slice of string
				c.Assert(result["title"], FitsTypeOf, []string{})
				// title must not be empty
				c.Assert(len(result["title"].([]string)), Not(Equals), 0)
				// first item of title value must not be an empty string
				c.Assert(len(result["title"].([]string)[0]), Not(Equals), 0)
			}

			if result["brand"] != nil {
				// brand to be a slice of string
				c.Assert(result["brand"], FitsTypeOf, []string{})
				// brand must not be empty
				c.Assert(len(result["brand"].([]string)), Not(Equals), 0)
				// first item of brand value must not be an empty string
				c.Assert(len(result["brand"].([]string)[0]), Not(Equals), 0)
			}

			if result["primaryImage"] != nil {
				// primaryImage to be a string
				c.Assert(result["primaryImage"], FitsTypeOf, "")
				// first item of primaryImage value must not be an empty string
				c.Assert(len(result["primaryImage"].(string)), Not(Equals), 0)
				// must be a link
				c.Assert(StartsWith("http", result["primaryImage"].(string)), Equals, true)
			}

			if result["otherImages"] != nil {
				// otherImages to be a slice of string
				c.Assert(result["otherImages"], FitsTypeOf, []string{})
				// when otherImages is not empty
				if len(result["otherImages"].([]string)) > 0 {
					firstItem := result["otherImages"].([]string)[0]
					// first item of otherImages must not be an empty string
					c.Assert(len(firstItem), Not(Equals), 0)
					// first item must be a url
					c.Assert(StartsWith("http", firstItem), Equals, true)
				}
			}

			if result["keyFeatures"] != nil {
				// keyFeatures to be a map of string key and interface{} value
				c.Assert(result["keyFeatures"], FitsTypeOf, map[string]interface{}{})
				// must not be empty
				c.Assert(len(result["keyFeatures"].(map[string]interface{})), Not(Equals), 0)
				// when keyFeatures contains `others` key, it should be an array of strings
				if result["keyFeatures"].(map[string]interface{})["others"] != nil {
					c.Assert(result["keyFeatures"].(map[string]interface{})["others"], FitsTypeOf, []string{})
				}
			}

			if result["fullFeatures"] != nil {
				// fullFeatures to be a map of string key and interface{} value
				c.Assert(result["fullFeatures"], FitsTypeOf, map[string]interface{}{})
				// must not be empty
				c.Assert(len(result["fullFeatures"].(map[string]interface{})), Not(Equals), 0)
				// when fullFeatures contains `others` key, it should be an array of strings
				if result["fullFeatures"].(map[string]interface{})["others"] != nil {
					c.Assert(result["fullFeatures"].(map[string]interface{})["others"], FitsTypeOf, []string{})
				}
			}

			if result["location"] != nil {
				// location to be a map of string key and string value
				c.Assert(result["location"], FitsTypeOf, map[string]string{})
				// location must not be empty
				c.Assert(len(result["location"].(map[string]string)), Not(Equals), 0)
			}

			if result["availableSizes"] != nil {
				// availableSizes to be a map of string key and interface{} value
				c.Assert(result["availableSizes"], FitsTypeOf, map[string]interface{}{})
				// availableSizes must not be empty
				c.Assert(len(result["availableSizes"].(map[string]interface{})), Not(Equals), 0)
				// availableSizes `sizes` key should not be empty
				c.Assert(len(result["availableSizes"].(map[string]interface{})["sizes"].([]string)), Not(Equals), 0)
				// availableSizes `unit` key should not be empty
				c.Assert(len(result["availableSizes"].(map[string]interface{})["unit"].(string)), Not(Equals), 0)
			}

			if result["productName"] != nil {
				// productName to be a slice of string
				c.Assert(result["productName"], FitsTypeOf, []string{})
				// productName must not be empty
				c.Assert(len(result["productName"].([]string)), Not(Equals), 0)
				// first item of productName value must not be an empty string
				c.Assert(len(result["productName"].([]string)[0]), Not(Equals), 0)
			}

			if result["regularPrice"] != nil {
				// must be a map of string key and map value
				c.Assert(result["regularPrice"], FitsTypeOf, map[string]interface{}{})
				// must contain currency 
				c.Assert(result["regularPrice"].(map[string]interface{})["currency"], NotNil)
				// must contain amount key
				c.Assert(result["regularPrice"].(map[string]interface{})["amount"], NotNil)
			}

			if result["specialPrice"] != nil {
				// must be a map of string key and map value
				c.Assert(result["specialPrice"], FitsTypeOf, map[string]interface{}{})
				// must contain currency 
				c.Assert(result["specialPrice"].(map[string]interface{})["currency"], NotNil)
				// must contain amount key
				c.Assert(result["specialPrice"].(map[string]interface{})["amount"], NotNil)
			}

			if result["longDescription"] != nil {
				// longDescription to be a string
				c.Assert(result["longDescription"], FitsTypeOf, "")
				// longDescription value must not be an empty string
				c.Assert(len(result["longDescription"].(string)), Not(Equals), 0)
			}

			// when shortDecription is present
			if result["shortDescription"] != nil {
				// shortDecription to be a string
				c.Assert(result["shortDescription"], FitsTypeOf, "")
				// shortDecription value must not be an empty string
				c.Assert(len(result["shortDescription"].(string)), Not(Equals), 0)
			}

			// when postedDate is present
			if result["postedDate"] != nil {
				// must be a string
				c.Assert(result["postedDate"], FitsTypeOf, "")
				// must be a RFC3339 date format
				validRFC3339Date, err := common.IsValidRFC3339Date(result["postedDate"].(string))
				c.Assert(err, IsNil)
				c.Assert(validRFC3339Date, Equals, true)
			}

			// when breadcrumbs is present
			if result["breadcrumbs"] != nil {
				// breadcrumbs to be a string
				c.Assert(result["breadcrumbs"], FitsTypeOf, "")
				// breadcrumbs value must not be an empty string
				c.Assert(len(result["breadcrumbs"].(string)), Not(Equals), 0)
			}
		} 
	}
}