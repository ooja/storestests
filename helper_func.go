package storestests

import (
	"bitbucket.org/ooja/crawler/fetcher"
	"gopkg.in/mgo.v2/bson"
	"regexp"
	"bitbucket.org/ooja/common"
	conn "bitbucket.org/ooja/crawler/connections"
)

// get all stores
func GetStores(storeNames []string, mongoDBName, mongoDBColName string) []common.Store {
    storeCol := conn.GetMongoDBConn().DB(mongoDBName).C(mongoDBColName)
	var stores []common.Store
	err := storeCol.Find(bson.M{ "name": bson.M{ "$in": storeNames }}).All(&stores)
	if err != nil {
		panic("unable to get stores: "+err.Error())
	}
	return stores
}

// fetch a url
func FetchURL(url string) (string, int, error) {

	// fetch the category sitemap url
    resp, err := fetcher.FetchURL(url)
   
    if nil != err {
        panic("Error downloading from " + url + ":" + err.Error())
    
    } else {

        defer resp.Body.Close()
        body, err := resp.Body.ToString();
        if err != nil {
            panic("Unable to read body of url ("+ url + ")")
        }

        return body, resp.StatusCode, err
    }
}

// get first key of a map of type map[string]struct{}
func GetFirstMapKey(m map[string]struct{}) string {
	for key, _ := range m {
		return key
	}
	return ""
}

func StartsWith(snippet, haystack string) bool {
	r, _ := regexp.MatchString("^" + snippet , haystack)
	return r
}