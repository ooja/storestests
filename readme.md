# StoresTest

Test all stores that are tightly supported by our parser. 

# List of Supported Stores

- KONGA
- JUMIA
- OLX
- KAYMU
- JAYOSBIE
- ELLA.NG
- DRINKS.NG
- PAYPORTE
- FASHPA
- SMARTBUY.NG

# Required Environment Variables

- `MONGO_USER`: Mongo db username
- `MONGO_PASS`: Mongo db password
- `APP_ENV`: Current environment name. Use `CI` when running in continous integration environment
- `LIMIT_TO`: Set a space seperated strings of stores to run test on. By default all stores are tested.